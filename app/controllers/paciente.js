import Ember from 'ember';

export default Ember.Controller.extend({
  // openModal: false,
  obrassociales: [
    'OSDE',
    'Swiss Medical',
    'Obra Social De Pasteleros',
    'Red Seguros Medicos',
    'Obra Social Del Personal De Prensa',
    'Sancor Salud',
    'Instituto De Prevision y Seguridad Social',
    'Union Obrera Metalurgica De La Rep Arg'
  ],

  localidades: [
    'Acheral',
    'Alto Verde',
    'Arcadia',
    'Barrio Aeropuerto',
    'Cebil Redondo',
    'Colombres',
    'Delfin Gallo',
    'El Chañar',
    'El Corte',
    'El Paraiso',
    'El Potrerillo',
    'Ex Ingenio Esperanza',
    'Ex Ingenio Lujan',
    'Gobernador Garmendia',
    'Ingenio La Florida',
    'Ingenio La Trinidad',
    'Ingenio San Pablo',
    'Ingenio Santa Barbara',
    'La Florida',
    'La Reduccion',
    'Lamadrid',
    'San Miguel de Tucuman'
  ],

  actions: {
    openForm: function () {
      console.log('openForm function');
      Ember.$('.ui.modal').modal('show');
    },

    approveModal: function () {
        
    },
    
    denyModal: function () {
      
    },

    updateAge: function () {
      console.log('** updateAge function **');
      console.log(new Date(this.get('nacimiento')));
      var ageDifMs = Date.now() - new Date(this.get('nacimiento')).getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      console.log(Math.abs(ageDate.getUTCFullYear() - 1970));
      var age = Math.abs(ageDate.getUTCFullYear() - 1970) + ' años';
      Ember.set(this, 'edad', age);
    }
  }
});