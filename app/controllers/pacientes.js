import Ember from 'ember';
var nombres = ("Ingeborg Jim Rigoberto Ian Wei Otilia Wendell Chasity " +
    "Billi Chester Omer Paulene Hiram Laurice Deneen Chuck Petrina Lamonica " +
    "Roy Ai Marsha Kiana Tamar Fabiola Travis Mazie Dawna Fe Tommye Jene").split(' ');

var apellidos = ("Brown Smith Patel Jones Williams Johnson Taylor Thomas " +
  "Roberts Khan Lewis Jackson Clarke James Phillips Wilson Ali Mason " +
  "Mitchell Rose Davis Davies Rodríguez Cox Alexander " +
  "Stewart Quinn Robinson Murphy Graham").split(' ');

var localidades = [
    'Acheral',
    'Alto Verde',
    'Arcadia',
    'Barrio Aeropuerto',
    'San Miguel de Tucuman',
    'Cebil Redondo',
    'Colombres',
    'Delfin Gallo',
    'San Miguel de Tucuman',
    'El Chañar',
    'El Corte',
    'El Paraiso',
    'San Miguel de Tucuman',
    'El Potrerillo',
    'Ex Ingenio Esperanza',
    'Ex Ingenio Lujan',
    'San Miguel de Tucuman',
    'San Miguel de Tucuman',
    'Gobernador Garmendia',
    'Ingenio La Florida',
    'Ingenio La Trinidad',
    'San Miguel de Tucuman',
    'Ingenio San Pablo',
    'Ingenio Santa Barbara',
    'La Florida',
    'La Reduccion',
    'Lamadrid',
    'San Miguel de Tucuman'];

var obrassociales = [
  'OSDE',
  'Swiss Medical',
  'Obra Social De Pasteleros',
  'Red Seguros Medicos',
  'Obra Social Del Personal De Prensa',
  'Sancor Salud',
  'Instituto De Prevision y Seguridad Social',
  'Union Obrera Metalurgica De La Rep Arg',
  'Obra Social Del Personal De La Sanidad Argentina',
  'Sancor Salud-obra social con cobertura nacional',
  'Accion Social de La Unt',
  'Asispre',
  'Sindicato de Trabajadores de Industrias de La Alimentacion',
  'Obra Social Del Personal de Sanidad Filia',
  'Instituto de Prevision y Seguridad Social',
  'Servicios Sociales Simoca',
  'Sindicato Petroleros de Tucuman',
  'Sociedad de Empleados y Obreros Del Comercio',
  'Osprera'];

var generateContent = function (recordsCount) {
  var ret = Ember.A([]);
  for (let i = 0; i < recordsCount; i++) {
    ret.push(Ember.Object.create({
      dni: Math.floor(Math.random()*10000000)*2,
      apellido: apellidos[i],
      nombre: nombres[i],
      obrasocial: obrassociales[i],
      edad: Math.floor(Math.random() * (50 - 18)) + 18,
      localidad: localidades[i],
      // cityWithHtml: '<i>' + cities[i] + '</i>'
    }));
  }
  return ret;
};

export default Ember.Controller.extend({
  columns: [
    {
      "propertyName": "dni",
      "title": "DNI"
    },
    {
      "propertyName": "apellido",
      "title": "Apellido"
    },
    {
      "propertyName": "nombre",
      "title": "Nombre"
    },
    {
      "propertyName": "obrasocial",
      "title": "Obra Social"
    },
    {
      "propertyName": "edad",
      "title": "Edad"
    },
    {
      "propertyName": "localidad",
      "title": "Localidad"
    }
  ],

  content: generateContent(30),

  customMessages: {
    "searchLabel": "Buscar",
    "columns-title": "Campos",
    "columns-showAll": "Mostrar todos",
    "columns-hideAll": "Ocultar todos",
    "columns-restoreDefaults": "Restaurar campos",
    "tableSummary": "Mostrando %@ - %@ de %@",
    "allColumnsAreHidden": "Todos los campos están ocultos.",
    "noDataToShow": "No se encontraron resultados..."
  }

});