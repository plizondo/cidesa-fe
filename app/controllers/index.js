import Ember from 'ember';

export default Ember.Controller.extend({
 
  occurrences: Ember.A([{
      title: '9:00 am | Juan Perez',
      startsAt: '2015-11-10 9:00',
      endsAt: '2015-11-10 10:00'
    },
    {
      title: '9:00 am | Juan Perez',
      startsAt: '2015-11-10 11:00',
      endsAt: '2015-11-10 11:30'
    },
    {
      title: '9:00 am | Juan Perez',
      startsAt: '2015-11-12 11:00',
      endsAt: '2015-11-12 11:30'
  }]),
  openModal: false,


  // Semantic-UI-Ember //
  pacientes: [
    'Juan Perez',
    'Fernanda Sanchez',
    'Juliana Banegas',
    'Juan Fuentes',
    'Diego Juarez',
    'Susana Salas',
    'Juan Perez',
    'Fernanda Sanchez',
    'Juliana Banegas',
    'Juan Fuentes',
    'Diego Juarez',
    'Susana Salas',
    'Juan Perez',
    'Fernanda Sanchez',
    'Juliana Banegas',
    'Juan Fuentes',
    'Diego Juarez',
    'Susana Salas'
  ],

  duracionConsulta: [
    '15 minutos',
    '30 minutos',
    '45 minutos',
    '60 minutos'
  ],

  // selectedAction: null,
  newTurno: null,
  /*newTurno: {
      title: '',
      startsAt: '',
      endsAt: ''
  },*/

  actions: {

    calendarNewOccurrence: function(occurrence) {
      console.log('**calendarNewOccurrence function**');
      console.log(occurrence);
      Ember.set(this, 'newTurno', occurrence);
      // Ember.set(this, 'newTurno.defaultOccurrenceDuration', '2:00');
      // Open a modal when click on calendar
      // Ember.set(this, 'newTurno.startsAt', occurrence.startsAt);
      // Ember.set(this, 'openModal', true);
      Ember.$('.ui.modal').modal('show');
    },

    calendarAddOccurrence: function () {
      console.log('**calendarAddOccurrence function**');
      this.get('occurrences').pushObject(Ember.Object.create({
        title: this.newTurno.get('title'),
        startsAt: this.newTurno.get('startsAt'),
        endsAt: this.newTurno.get('endsAt')
      }));
      // Close modal
      // Ember.set(this, 'openModal', false);
      Ember.$('.ui.modal').modal('hide');
    },

    calendarUpdateOccurrence: function(occurrence, properties) {
      occurrence.setProperties(properties);
    },

    calendarRemoveOccurrence: function(occurrence) {
      this.get('occurrences').removeObject(occurrence);
    },

    updateSelectedPatient: function(component, id, value) {
      console.log('//***updateSelectedPatient function ***//');
      // Ember.set(this, 'selectedAction', id);
      Ember.set(this, 'newTurno.title', value);
      console.log(this.newTurno);
      console.log(component);
      console.log(id);
      console.log(value);
    },

    updateSelectedHour: function(component, id, value) {
      console.log('//***updateSelectedHour function ***//');
      // this.set('selectedAction', id);
    },

    approveModal: function () {
        
    },
    
    denyModal: function () {
      
    }
  }
});