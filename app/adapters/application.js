import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  namespace: 'HayTurno/services',
  host: 'http://localhost:8095'
  // host: 'http://hayturno.getsandbox.com'
});
