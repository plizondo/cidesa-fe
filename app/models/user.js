import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr(),
  email: DS.attr(),
  birthDate: DS.attr(),
  city: DS.attr(),
  state: DS.attr()
});
