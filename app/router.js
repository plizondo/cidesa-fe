import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('paciente');
  this.route('pacientes');
  this.resource('user');
  this.resource('provincia');
});

export default Router;
